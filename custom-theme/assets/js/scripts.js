(function ($) {
    $(document).ready(function(){
       console.log('scripts ready');

    $('.color-select').on("click", function() {
        $('.color-select.selected').removeClass('selected');
        $(this).toggleClass('selected');
    });

    $('.add-to-cart-button').on("click", function(event) {
        event.preventDefault();
        let button = $(this);
        let id = $(this).val();
        let color = $(this).parent().parent().parent().find('.color-select.selected').val();
        $.ajax({
            type: "post",
            url: "wp-admin/admin-ajax.php",
            data: {
                action: "custom_add_to_cart",
                product_id: id,
                color: color
            },
            success: function (response) {
                if(response.data.success === true){
                    button.html('Product added');
                }
                else if(response.data.error == 'select color') {
                    button.html('Choose the color');
                }
            }
        });
    });
    });

})(jQuery);