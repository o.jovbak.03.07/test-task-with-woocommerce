<?php
define('TEXTDOMAIN', 'custom-theme');

function enqueue_styles(){
    wp_enqueue_style('main_css', get_template_directory_uri() . '/assets/css/main-styles.css');
}
add_action( 'wp_enqueue_scripts', 'enqueue_styles' );

function enqueue_scripts(){
    wp_enqueue_script('scripts', get_template_directory_uri() . '/assets/js/scripts.js', array('jquery'), false, true);
}
add_action( 'wp_enqueue_scripts', 'enqueue_scripts' );

//custom adding products to cart with ajax
add_action('wp_ajax_custom_add_to_cart', 'ajax_custom_add_to_cart');
add_action('wp_ajax_nopriv_custom_add_to_cart', 'ajax_custom_add_to_cart');

function ajax_custom_add_to_cart() {
    $product_id = $_POST['product_id'];
    $product = wc_get_product($product_id);
    $result = [];
    if ($product->is_type( 'simple' )) {
        WC()->cart->add_to_cart($product_id, 1);
        $result = ['success' => true];
    }
    elseif($product->is_type('variable')){
        if($_POST['color']) {
            $args['color'] = $_POST['color'];
            WC()->cart->add_to_cart($product_id, 1, 0, $args);
            $result = ['success' => true];
        }
        else {
            $result = ['error' => 'select color'];
        }
    }

    wp_send_json_success($result);
}

//solution with mail for Contact form 7 and custom post type

add_action('init', 'my_custom_post_types');

function my_custom_post_types() {
    register_post_type('members', array(
        'labels' => array(
            'name' => 'Members',
            'singular_name' => 'Member',
            'add_new' => 'Add new',
            'add_new_item' => 'Add new memeber',
            'edit_item' => 'Edit member',
            'new_item' => 'New member',
            'all_items' => 'All members',
            'view_item' => 'View member',
            'search_items' => 'Search members',
            'not_found' =>  'No awards found',
            'not_found_in_trash' => 'No awards in trash',
            'menu_name' => 'Members'
        ),
        'public' => true,
        'publicly_queryable' => false,
        'show_in_rest' => true,
        'menu_icon' => 'dashicons-groups',
        'has_archive' => false,
        'pages' => false,
        'with_front' => false,
        'supports' => array( 'title'),
        'rewrite' => array( 'with_front' => false)
    ));
}

add_action("wpcf7_before_send_mail", "save_memebers_in_database");

function save_memebers_in_database($WPCF7_ContactForm) {
    if (91 == $WPCF7_ContactForm->id()) { //add the right contact form id

        $wpcf7      = WPCF7_ContactForm::get_current();
        $submission = WPCF7_Submission::get_instance();

        if ($submission) {
            $data = $submission->get_posted_data();

            if (empty($data))
                return;

            $new_member = [
                'post_title' => $data['email'],
                'post_type' => 'members',
                'meta_input' => [
                    'date' => current_time('d/m/Y H:i')
                ],
            ];

            wp_insert_post($new_member);
            return $wpcf7;
        }
    }
}