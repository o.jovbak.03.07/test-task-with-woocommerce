<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package creativeboost
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <title><?php the_title();?></title>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div class="custom_overlay"></div>
<div id="page" class="site">

    <header id="masthead" class="site-header" role="banner">
        <div class="header-wrap">
            <div class="container">
                <div class="row mobile-header">
                    <div class="col-md-2 col-sm-3 col-xs-4">
                        <a href="<?php echo esc_url(home_url('/')); ?>" title="<?php bloginfo('name'); ?>"></a>
                    </div>
                    <div class="col-xs-12">
                        <nav id="mainnav" class="mainnav" role="navigation">
                            <?php wp_nav_menu([
                                'menu'              => 'main menu'
                            ]); ?>
                        </nav><!-- #site-navigation -->
                    </div>
                </div>

            </div>
        </div>
    </header><!-- #masthead -->

