<?php
get_header(); ?>

    <?php if(class_exists( 'WooCommerce' )): ?>
    <section class="products-list" >
        <div class="container">
            <?php
            $args = array(
                'post_type' => 'product',
                'post_status' => 'publish'
            );

            $products = new WP_Query( $args );

            if ($products->have_posts()) : ?>
                <div class="row">
                    <?php while ($products->have_posts()) :
                        $products->the_post(); ?>
                        <?php get_template_part('template-parts/product-for-list', '', ['product' => wc_get_product( $products->post->ID )]); ?>
                    <?php endwhile; ?>
                </div>

            <?php endif; ?>

            <?php wp_reset_query();
            ?>
        </div>
    </section>

    <?php else: ?>
        <div class="container">
            <div class="row">
                <div class="blog-posts col-md-8">
                    <?php if ( have_posts() ): ?>
                        <?php while( have_posts() ): ?>
                            <?php the_post(); ?>
                            <div class="blog-post">
                                <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                                <?php if ( has_post_thumbnail() ) :
                                    $featured_image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'medium' ); ?>
                                    <div class="blog-post-thumb">
                                        <a href="<?php the_permalink(); ?>"><img src="<?php echo $featured_image[0]; ?>" alt='' /></a>
                                    </div>
                                <?php endif; ?>
                                <?php the_excerpt(); ?>
                                <a class="read-more-link" href="<?php the_permalink(); ?>"><?php _e( 'Read More' ); ?></a>
                            </div>
                        <?php endwhile; ?>
                    <?php else: ?>
                        <p><?php _e( 'No Blog Posts found', 'custom-theme' ); ?></p>
                    <?php endif; ?>
                </div>
                <div id="blog-sidebar" class="col-md-4">

                </div>
            </div>
        </div>
    <?php endif; ?>
<?php get_footer(); ?>