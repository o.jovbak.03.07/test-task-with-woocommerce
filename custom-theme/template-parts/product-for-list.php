<?php
/**
 * @var  $args ;
 */

$product = $args['product'];
$regular_price = '';
$sale_price = false;
$discount = false;
$variations = [];
$colors = [];
if ($product->is_type( 'simple' )) {
        $sale_price     =  $product->get_sale_price() ? $product->get_sale_price() : false;
        $regular_price  =  $product->get_regular_price();
    }
elseif($product->is_type('variable')){
    $sale_price     =  $product->get_variation_sale_price( 'min', true ) ? $product->get_variation_sale_price( 'min', true ) : false;
    $regular_price  =  $product->get_variation_regular_price( 'max', true );
    $variations = $product->get_available_variations();
}

if($sale_price) {
    $discount = round (100 - ($sale_price / $regular_price * 100 ));
}

if(!empty($variations)){
    foreach ($variations as $variation) {
        if($variation['attributes']['attribute_color']) {
            $colors[] = $variation['attributes']['attribute_color'];
        }
    }
}

?>
<div class="col-md-4 custom-single-product">
    <div class="custom-single-product__img-block" style="background-image: url('<?= get_the_post_thumbnail_url() ?>')">
        <?php if(false !== $discount): ?>
            <div class="img-block__discount">
                <div class="img-block__discount-value">
                    <span>
                        <?= $discount > 0 ? '-'.$discount.'%' : 'Sale' ?>
                    </span>
                </div>
            </div>
        <?php endif; ?>
        <div class="img-block__add-to-cart">
            <button class="add-to-cart-button" value="<?= get_the_ID() ?>">
                Add to cart
            </button>
        </div>
    </div>
    <div class="custom-single-product__description-block">
        <div class="description-block__price-row">
            <div class="description-block__prices">
                <?php if(false !== $discount): ?>
                    <span class="price">
                        <?= '$'.$sale_price; ?>
                    </span>
                    <span class="old-price">
                        <?= '$'.$regular_price; ?>
                    </span>
                <?php else: ?>
                    <span class="price">
                        <?= '$'.$regular_price; ?>
                    </span>
                <?php endif; ?>
            </div>
            <div class="description-block__wishlist">
                <i class="bi bi-heart"></i>
            </div>
        </div>
        <div class="description-block__title">
            <a href="<?= get_permalink() ?>">
                <?= $product->get_title() ?>
            </a>
        </div>
        <?php if(!empty($colors)): ?>
            <div class="description-block__variations">
                <?php foreach ($colors as $color): ?>
                    <button class="color-select" style="background-color: <?= $color?>" value="<?= $color?>">
                    </button>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    </div>
</div>
